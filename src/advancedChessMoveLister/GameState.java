package advancedChessMoveLister;

public class GameState {
	private boolean[] castleAbility;
	private Square enPassant;
	private int halfMovesSinceLastPawnMoveOrCapture;
	private int fullMoves;

	public GameState(boolean[] castling, Square enPassant, int halfMoves,
			int fullMoves) {
		this.castleAbility = castling;
		this.enPassant = enPassant;
		this.halfMovesSinceLastPawnMoveOrCapture = halfMoves;
		this.fullMoves = fullMoves;
	}

	public boolean[] getCastling() {
		return castleAbility;
	}

	public Square getEnPassantCopy() {
		if(enPassant==null)
			return null;
		else
		return new Square(enPassant.getRow(), enPassant.getColumn());
	}

	public int getHalfMoves() {
		return halfMovesSinceLastPawnMoveOrCapture;
	}

	public int getFullMoves() {
		return fullMoves;
	}
	
	public String toString(){
		String castleString = "";
		if(castleAbility[3])
			castleString=castleString+"K";
		if(castleAbility[2])
			castleString=castleString+"Q";
		if(castleAbility[1])
			castleString=castleString+"k";
		if(castleAbility[0])
			castleString=castleString+"q";
		if(castleString.length()==0)
			castleString="-";
		
		String e="";
		char[] rep = new char[2];
		if(enPassant==null)
			e="-";
		else{
		
			rep[0] = (char)(enPassant.getColumn()+97);
			rep[1] = (char)(enPassant.getRow()+49);
			e=new String(rep);
		}
		
		return castleString+" "+e+" "+halfMovesSinceLastPawnMoveOrCapture+" "+fullMoves;
	}

	// ///////private methods here:

}
