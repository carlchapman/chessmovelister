package advancedChessMoveLister;

public class EnPassantMove extends PawnMove {

	public EnPassantMove(ChessBoard boardBeforeMove, Piece actor,
			Square destination) {
		super(boardBeforeMove, actor, destination);
		Piece[][] newSetup = super.getBoardAfterMove().getPieces();
		if (super.getActor().isWhite()) {
			newSetup[4][super.getDestination().getColumn()] = null;
		} else {
			newSetup[3][super.getDestination().getColumn()] = null;
		}
		super.getBoardAfterMove().setPieces(newSetup);

	}
}
