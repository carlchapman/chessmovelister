package advancedChessMoveLister;

import java.util.Scanner;

public class PawnPromotionMove extends PawnMove {

	public PawnPromotionMove(ChessBoard boardBeforeMove, Piece actor,
			Square destination) {
		super(boardBeforeMove, actor, destination);
	}

	public void selectPromotion(String newline) {
		Piece[][] pieces = super.getBoardAfterMove().getPieces();

		Scanner inLine = new Scanner(System.in);
		inLine.useDelimiter(newline);
		boolean selecting = true;
		while (selecting) {
			System.out
					.println("Please select one of: queen rook bishop knight (and then press Enter)");
			String selection = inLine.next();
			if (selection.contains("queen")) {
				selecting = false;
				pieces[super.getDestination().getRow()][super.getDestination()
						.getColumn()] = new Queen(super.getActor().isWhite(),
						super.getDestination());
			} else if (selection.contains("rook")) {
				selecting = false;
				pieces[super.getDestination().getRow()][super.getDestination()
						.getColumn()] = new Rook(super.getActor().isWhite(),
						super.getDestination());
			} else if (selection.contains("bishop")) {
				selecting = false;
				pieces[super.getDestination().getRow()][super.getDestination()
						.getColumn()] = new Bishop(super.getActor().isWhite(),
						super.getDestination());
			} else if (selection.contains("knight")) {
				selecting = false;
				pieces[super.getDestination().getRow()][super.getDestination()
						.getColumn()] = new Knight(super.getActor().isWhite(),
						super.getDestination());
			} else {
				System.out
						.println("Your selection did not match one of the options.");
			}
		}
	}
}
