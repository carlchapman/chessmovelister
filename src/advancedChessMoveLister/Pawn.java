package advancedChessMoveLister;

import java.util.Vector;

public class Pawn extends Piece {
	public Pawn(boolean isWhite, Square initialPosition) {
		super(isWhite, initialPosition);
	}

	public Piece getCopyInNewSquare(Square newSquare) {
		return new Pawn(this.isWhite(), newSquare);
	}

	public String toString() {
		return "pawn at " + this.getSquare().toString();
	}

	public String getLetter() {
		return (this.isWhite()) ? "P" : "p";
	}

	@Override
	public Vector<Move> getPossibleMoves(ChessBoard board, GameState gameState) {
		int rowNumber = this.getSquare().getRow();
		int columnNumber = this.getSquare().getColumn();
		Vector<Move> moves = new Vector<Move>();
		if (this.isWhite()) {
			try {
				Square forwardOne = new Square(rowNumber + 1, columnNumber);
				if (board.getPiece(forwardOne) == null) {
					if (rowNumber == 6)
						moves
								.add(new PawnPromotionMove(board, this,
										forwardOne));
					else
						moves.add(new PawnMove(board, this, forwardOne));
					if (rowNumber == 1) {
						Square forwardTwo = new Square(3, columnNumber);
						if (board.getPiece(forwardTwo) == null)
							moves.add(new PawnMove(board, this, forwardTwo));
					}
				}
			} catch (Exception e) {
			}
		} else {
			try {
				Square forwardOne = new Square(rowNumber - 1, columnNumber);
				if (board.getPiece(forwardOne) == null) {
					if (rowNumber == 1)
						moves
								.add(new PawnPromotionMove(board, this,
										forwardOne));
					else
						moves.add(new PawnMove(board, this, forwardOne));
					if (rowNumber == 6) {
						Square forwardTwo = new Square(4, columnNumber);
						if (board.getPiece(forwardTwo) == null)
							moves.add(new PawnMove(board, this, forwardTwo));
					}
				}
			} catch (Exception e) {

			}
		}
		Vector<Square> squares = getAttackedSquares(board);
		
		for(Square x:squares){
			if((board.squareIsOccupied(x)&&(this.isWhite()!=board.getPiece(x).isWhite())))
				moves.add(new CaptureMove(board,this,x));
			else if(x.equals(gameState.getEnPassantCopy())){
				moves.add(new EnPassantMove(board,this,x));
			}
		}
		return moves;
	}

	public Vector<Square> getAttackedSquares(ChessBoard board) {
		Vector<Square> squares = new Vector<Square>();
		int rowNumber = this.getSquare().getRow();
		int columnNumber = this.getSquare().getColumn();
		if (this.isWhite()) {
			try {
				Square northWest = new Square(rowNumber + 1, columnNumber - 1);
					squares.add(northWest);
			} catch (Exception e) {

			}
			try {
				Square northEast = new Square(rowNumber + 1, columnNumber + 1);
					squares.add(northEast);
			} catch (Exception e) {

			}

			// for black Pawns...
		} else {

			try {
				Square southWest = new Square(rowNumber - 1, columnNumber - 1);
					squares.add(southWest);
			} catch (Exception e) {

			}
			try {
				Square southEast = new Square(rowNumber - 1, columnNumber + 1);
					squares.add(southEast);
			} catch (Exception e) {

			}
		}
		return squares;
	}
}
