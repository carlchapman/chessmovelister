package advancedChessMoveLister;

import java.util.Vector;

public class ChessBoard {
	private Piece[][] pieces;
	private boolean whiteIsNext;

	public ChessBoard(Piece[][] pieces, boolean whiteIsNext) {
		this.pieces = pieces;
		this.whiteIsNext = whiteIsNext;
	}

	public Piece[][] getPiecesCopy() {
		Piece[][] piecesToReturn = new Piece[8][8];
		for(int i=0;i<pieces.length;i++)
			for(int j=0;j<pieces[i].length;j++){
				piecesToReturn[i][j]=this.pieces[i][j];
			}
		return piecesToReturn;
	}

	public boolean getWhiteIsNext() {
		return whiteIsNext;
	}
	
	public Piece[][] getPieces(){
		return pieces;
	}
	
	public void setPieces(Piece[][] newSetup){
		this.pieces = newSetup;
	}
	
	public Piece getPiece(Square target){
		return pieces[target.getRow()][target.getColumn()];
	}
	
	public Piece getPiece(int row, int column){
		return pieces[row][column];
	}
	
	public boolean squareIsOccupied(Square target){
		return !(pieces[target.getRow()][target.getColumn()]==null);
		
	}

	public Vector<Square> findAttackedSquares() {
		Vector<Square> attackedSquares = new Vector<Square>();
		for(int i=0;i<8;i++)
			for(int j=0;j<8;j++){
				Piece p = pieces[i][j];
				if(p!=null&&p.isWhite()==whiteIsNext)
					attackedSquares.addAll(p.getAttackedSquares(this));
			}
		return attackedSquares;
	}

	public String toString() {
		String board="";
		for (int i = 7; i >= 0; i--) {
			String rank="";
			int emptySpace =0;
			for (int j = 0; j < 8; j++) {
				Piece p = pieces[i][j];
				if(p==null){
					emptySpace++;
					if(j==7)
						rank=rank+emptySpace;
				}else {
					if(emptySpace>0){
						rank=rank+emptySpace;
						emptySpace=0;
					}
					rank=rank+p.getLetter();
				}
			}
			if(i==0)
				board=board+rank;
			else
				board=board+rank+"/";
		}
		if(whiteIsNext)
			board=board+" w";
		else
			board=board+" b";
		return board;
	}
}
