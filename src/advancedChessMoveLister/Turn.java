package advancedChessMoveLister;

import java.util.Scanner;
import java.util.Vector;

public class Turn {
	private ChessBoard board;
	private GameState gameState;

	public Turn(ChessBoard board, GameState gameState) {
		this.board = board;
		this.gameState = gameState;
	}

	public Turn(String fenRepresentation) {

		// break down the fen into its 6 parts and use them to initialize
		// variables
		Scanner in = new Scanner(fenRepresentation);

		// 1
		String boardString = in.next();
		// 2
		String whoseTurn = in.next();
		boolean whiteIsNext = (whoseTurn.charAt(0) == 'w')
				|| (whoseTurn.charAt(0) == 'W');
		board = new ChessBoard(findPiecesOnBoard(boardString), whiteIsNext);
		// 3
		String castling = in.next();
		boolean[] castleAbility = findCastleAbility(castling);
		// 4
		String enPassant = in.next();
		Square enPassantSquare = findEnPassant(enPassant);
		// 5
		int halfMovesSinceLastPawnMoveOrCapture = in.nextInt();
		// 6
		int fullMoves = in.nextInt();
		gameState = new GameState(castleAbility, enPassantSquare,
				halfMovesSinceLastPawnMoveOrCapture, fullMoves);
	}

	public void printMoves() {
		Vector<NumberedMove> legalMoves = getLegalMoves();
		for (NumberedMove x : legalMoves)
			System.out.println(x.toString());
		System.out.println();
		printBoard();
		System.out.println();
		System.out.println(getFen());
		System.out.println();
	}

	public String getFen() {
		return board.toString()+" "+gameState.toString();
	}

	public Vector<NumberedMove> getLegalMoves() {
		Vector<NumberedMove> legalMoves = new Vector<NumberedMove>();
		Vector<Move> possibleMoves = new Vector<Move>();
		int n=1;
		for(int i=0;i<8;i++)
			for(int j=0;j<8;j++){
				Piece p = board.getPiece(i,j);
				if(p!=null&&(p.isWhite()==board.getWhiteIsNext()))
					possibleMoves.addAll(p.getPossibleMoves(board, gameState));
			}
		for(Move x: possibleMoves)
			if(x.isLegal())
				legalMoves.add(new NumberedMove(x,n++));
		return legalMoves;
	}
	
	public GameState getGameState(){
		return gameState;
	}

	/////////////////////////////////////////////////////////
	
	private void printBoard() {
		System.out.println("   ---------------------------------");
		for (int i = 7; i >= 0; i--) {
			System.out.print(" "+(i + 1)+" ");
			for (int j = 0; j < 8; j++) {
				System.out.print("| ");
				Piece p = board.getPiece(i,j);
				if (p == null
						&& ((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1)))
					System.out.print("* ");
				else if (p == null)
					System.out.print("  ");
				else
					System.out.print(p.getLetter() + " ");
			}
			System.out.print("|");
			System.out.println();
			System.out.println("   ---------------------------------");
		}
		System.out.println("     a   b   c   d   e   f   g   h");
	}
	private boolean[] findCastleAbility(String castling) {
		// 0=black queenside, 1= black kingside
		// 2=white queenside, 3= white kingside
		boolean[] castleAbility = new boolean[4];
		castleAbility[0] = castling.contains("q");
		castleAbility[1] = castling.contains("k");
		castleAbility[2] = castling.contains("Q");
		castleAbility[3] = castling.contains("K");
		return castleAbility;

	}

	private Square findEnPassant(String enPassant) {
		if (enPassant.contains("-"))
			return null;
		else {

			// this assumes lowercase letters were entered
			int column = (int) enPassant.charAt(0) - 97;
			int row = (int) enPassant.charAt(1) - 48;

			return new Square(row, column);
		}
	}

	private Piece[][] findPiecesOnBoard(String board) {
		Piece[][] pieces = new Piece[8][8];
		Scanner parse = new Scanner(board);
		parse.useDelimiter("/");
		for (int i = 7; i >= 0; i--) {
			int column = 0;
			String rank = parse.next();
			for (int j = 0; j < rank.length(); j++) {
				char info = rank.charAt(j);

				// if it's a number
				if ((int) info > 47 && (int) info < 56) {

					// increment the column cursor by that number
					column += (int) info - 48;

					// otherwise if it's lowercase
				} else if ((int) info > 96 && (int) info < 115)
					pieces[i][column] = getDesiredPiece(info, false, i,
							column++);
				else
					pieces[i][column] = getDesiredPiece(info, true, i, column++);
			}
		}
		return pieces;
	}

	private Piece getDesiredPiece(char c, boolean isWhite, int row, int column) {
		Piece desiredPiece = null;
		if (c == 'p' || c == 'P')
			desiredPiece = new Pawn(isWhite, new Square(row, column));
		if (c == 'k' || c == 'K')
			desiredPiece = new King(isWhite, new Square(row, column));
		if (c == 'b' || c == 'B')
			desiredPiece = new Bishop(isWhite, new Square(row, column));
		if (c == 'n' || c == 'N')
			desiredPiece = new Knight(isWhite, new Square(row, column));
		if (c == 'q' || c == 'Q')
			desiredPiece = new Queen(isWhite, new Square(row, column));
		if (c == 'r' || c == 'R')
			desiredPiece = new Rook(isWhite, new Square(row, column));
		return desiredPiece;
	}
}
