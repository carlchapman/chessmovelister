package advancedChessMoveLister;


	public class PawnMove extends Move {
		public PawnMove(ChessBoard boardBeforeMove, Piece actor,
				Square destination) {
			super(boardBeforeMove, actor, destination);
		}

		public GameState getNewGameState(GameState oldGameState) {
			Square enPassant = null;
			//if it was a white pawn move going 2 squares
			if(!super.getBoardAfterMove().getWhiteIsNext()&&super.getActor().getSquare().getRow()==1&&super.getDestination().getRow()==3)
				enPassant=new Square(2,super.getActor().getSquare().getColumn());
			
			//otherwise if it was a black pawn move going 2 squares...
			else if(super.getBoardAfterMove().getWhiteIsNext()&&super.getActor().getSquare().getRow()==6&&super.getDestination().getRow()==4)
				enPassant = new Square(5, super.getActor().getSquare().getColumn());
				
			GameState gameStateAfterMove = super.getNewGameState(oldGameState);
			gameStateAfterMove = new GameState(
					gameStateAfterMove.getCastling(),
					enPassant, 0, gameStateAfterMove.getFullMoves());


			return gameStateAfterMove;
		}
}
