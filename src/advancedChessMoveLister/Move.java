package advancedChessMoveLister;

public class Move {
	private Piece actor;
	private Square destination;
	private ChessBoard boardAfterMove;

	// later each move can be worth a certain amount of points...

	public Move(ChessBoard boardBeforeMove, Piece actor, Square destination) {
		this.actor = actor;
		this.destination = destination;
		Piece[][] pieces = boardBeforeMove.getPiecesCopy();
		pieces[actor.getSquare().getRow()][actor.getSquare().getColumn()] = null;
		pieces[destination.getRow()][destination.getColumn()] = actor
				.getCopyInNewSquare(destination);
		boardAfterMove = new ChessBoard(pieces, !boardBeforeMove
				.getWhiteIsNext());

	}

	public Piece getActor() {
		return actor;
	}

	public Square getDestination() {
		return destination;
	}

	public GameState getNewGameState(GameState oldGameState) {
		int fullMoves = oldGameState.getFullMoves();
		if (boardAfterMove.getWhiteIsNext())
			fullMoves++;

		// this default new gamestate is correct if there were no pawn
		// moves and no captures
		GameState gameStateAfterMove = new GameState(
				getCastlingAfterMove(oldGameState.getCastling()), null,
				oldGameState.getHalfMoves() + 1, fullMoves);
		return gameStateAfterMove;
	}

	public ChessBoard getBoardAfterMove() {
		return boardAfterMove;
	}

	public String toString() {
		return actor.toString() + " to " + destination.toString();
	}

	public boolean isLegal() {
		Square kingSquare = null;
		Piece[][] pieces = boardAfterMove.getPieces();
		
		//if this is a white move then check the white king square
		if(!boardAfterMove.getWhiteIsNext()){
			for(int i=0;i<8;i++)
				for(int j=0;j<8;j++){
					Piece p=pieces[i][j];
					if(p!=null&&p.getClass()==King.class&&p.isWhite())
						kingSquare =pieces[i][j].getSquare();
				}
			
			//otherwise, check the black king square
		}else
			for(int i=0;i<8;i++)
				for(int j=0;j<8;j++){
					Piece p = pieces[i][j];
					if(p!=null&&p.getClass()==King.class&&!p.isWhite())
						kingSquare =pieces[i][j].getSquare();
				}
		return !boardAfterMove.findAttackedSquares().contains(kingSquare);
	}

	// //////////////////////////private methods...////////////////////////
	private boolean[] getCastlingAfterMove(boolean[] oldCastling) {
		boolean[] toReturn = new boolean[4];
		Square blackQueenSideCorner = new Square(7, 0);
		Square blackKingSideCorner = new Square(7, 7);
		Square whiteQueenSideCorner = new Square(0, 0);
		Square whiteKingSideCorner = new Square(0, 7);

		boolean blackKingMoved = (actor.getClass() == King.class && !actor
				.isWhite());
		boolean whiteKingMoved = (actor.getClass() == King.class && actor
				.isWhite());
		boolean blackQueenSideRookMoved = (actor.getClass() == Rook.class
				&& !actor.isWhite() && actor.getSquare().equals(
				blackQueenSideCorner));
		boolean blackKingSideRookMoved = (actor.getClass() == Rook.class
				&& !actor.isWhite() && actor.getSquare().equals(
				blackKingSideCorner));
		boolean whiteQueenSideRookMoved = (actor.getClass() == Rook.class
				&& actor.isWhite() && actor.getSquare().equals(
				whiteQueenSideCorner));
		boolean whiteKingSideRookMoved = (actor.getClass() == Rook.class
				&& actor.isWhite() && actor.getSquare().equals(
				whiteKingSideCorner));

		// this should cover castling moves because the king is the actor who
		// moves
		toReturn[0] = oldCastling[0] && !blackKingMoved
				&& !blackQueenSideRookMoved;
		toReturn[1] = oldCastling[1] && !blackKingMoved
				&& !blackKingSideRookMoved;
		toReturn[2] = oldCastling[2] && !whiteKingMoved
				&& !whiteQueenSideRookMoved;
		toReturn[3] = oldCastling[3] && !whiteKingMoved
				&& !whiteKingSideRookMoved;

		return toReturn;
	}

}
