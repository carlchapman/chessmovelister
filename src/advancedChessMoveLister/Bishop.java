package advancedChessMoveLister;

import java.util.Vector;

public class Bishop extends Piece{
	
	public Bishop(boolean isWhite, Square initialPosition) {
		super(isWhite, initialPosition);
	}
	
	public Piece getCopyInNewSquare(Square newSquare){
		return new Bishop(this.isWhite(),newSquare);
	}
	
	public String toString(){
		return "bishop at "+this.getSquare().toString();
	}
	
	public String getLetter(){
		return (this.isWhite())?"B":"b";
	}
	
	public Vector<Square> getAttackedSquares(ChessBoard board) {

		Vector<Square> moves = new Vector<Square>();
		int number = this.getSquare().getRow();
		int letter = this.getSquare().getColumn();
		
		//check NorthEast
		boolean clear = true;
		int x=1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number + x++,letter+(x-1));
				Piece p = board.getPiece(s);
				if (p == null)
					moves.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					moves.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		//check NorthWest
		clear = true;
		x=1;
		int y=-1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number+x++,letter+y--);
				Piece p = board.getPiece(s);
				if (p == null)
					moves.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					moves.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		//check SouthWest
		clear = true;
		x=-1;
		y=1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number + x--,letter+y++);
				Piece p = board.getPiece(s);
				if (p == null)
					moves.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					moves.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		//check SouthEast
		clear = true;
		x=-1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number+x-- ,letter+x+1);
				Piece p = board.getPiece(s);
				if (p == null)
					moves.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					moves.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		return moves;
	}
}
