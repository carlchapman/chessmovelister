package advancedChessMoveLister;

public class CaptureMove extends Move {
	public CaptureMove(ChessBoard boardBeforeMove, Piece actor,
			Square destination) {
		super(boardBeforeMove, actor, destination);
	}

	public GameState getNewGameState(GameState oldGameState) {

		GameState gameStateAfterMove = super.getNewGameState(oldGameState);
		gameStateAfterMove = new GameState(
				getCastlingAfterCapture(gameStateAfterMove.getCastling()),
				null, 0, gameStateAfterMove.getFullMoves());

		return gameStateAfterMove;
	}

	//////////////
	private boolean[] getCastlingAfterCapture(boolean[] oldCastling) {
		boolean[] newCastling = new boolean[4];
		Square blackQueenSideCorner = new Square(7, 0);
		Square blackKingSideCorner = new Square(7, 7);
		Square whiteQueenSideCorner = new Square(0, 0);
		Square whiteKingSideCorner = new Square(0, 7);
		newCastling[0] = oldCastling[0]
				&& !(blackQueenSideCorner.equals(super.getDestination()));
		newCastling[1] = oldCastling[1]
				&& !(blackKingSideCorner.equals(super.getDestination()));
		newCastling[2] = oldCastling[2]
				&& !(whiteQueenSideCorner.equals(super.getDestination()));
		newCastling[3] = oldCastling[3]
				&& !(whiteKingSideCorner.equals(super.getDestination()));
		return newCastling;
	}
}
