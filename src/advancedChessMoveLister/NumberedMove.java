package advancedChessMoveLister;

public class NumberedMove {
	private Move move;
	private int IDNumber;

	public NumberedMove(Move move, int IDNumber) {
		this.move = move;
		this.IDNumber = IDNumber;
	}

	public int getIDNumber() {
		return IDNumber;
	}

	public Move getMove() {
		return move;
	}
	
	public String toString(){
		return IDNumber+": "+move.toString();
	}
}
