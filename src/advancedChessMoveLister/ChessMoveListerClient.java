package advancedChessMoveLister;

import java.util.Scanner;
import java.util.Vector;

public class ChessMoveListerClient {
	public static String newline = System.getProperty("line.separator");

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		System.out
				.println("This program takes a String of text in Forsyth-Edwards Notation (FEN) and outputs ");
		System.out
				.println("all legal moves.  Would you like a brief explanation of FEN notation? (Enter Y or N)");
		String choice = in.nextLine();
		if (choice.contains("y") || choice.contains("Y")) {
			System.out
					.println("FEN notation contains six fields seperated by spaces.");
			System.out
					.println("The first field describes the state of the board using capital letters for white,");
			System.out
					.println("lowercase for black, numbers to describe empty squares in a row and forward slashes");
			System.out
					.println("to separate rows.  The second field is a single letter w or b indicating whose turn is next.");
			System.out
					.println("The third field lists the sides of the board that still allow castling for each player");
			System.out
					.println("or just a dash if there are none).  The fourth field is the algebreic notation of the");
			System.out
					.println("en-passant target square, or just a dash if there is none.  The fifth field is how many");
			System.out
					.println("half-moves have taken place since the last pawn move or capture.  The sixth field counts");
			System.out
					.println("the number of full moves starting at 1 and incrementing whenever it is white's turn.");
			System.out.println();
			System.out
					.println("An example of FEN notation before any pieces have been moved is:");
			System.out.println();
			System.out
					.println("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
		}

		System.out
				.println("Please input the String describing the state of the chess game, then press Enter.");
		Scanner inLine = new Scanner(System.in);
		inLine.useDelimiter(newline);
		String fen = inLine.next();

		Vector<Turn> history = new Vector<Turn>();
		Turn beginning = new Turn(fen);
		history.add(beginning);
		beginning.printMoves();
		boolean playing = true;
		while (playing) {
			System.out
					.println("Please input a move number to play from this position,");
			System.out
					.println("B to go back one move or Q to quit (and then press Enter).");
			String selection = inLine.next();
			if (selection.contains("Q") || selection.contains("q"))
				playing = false;
			else if (selection.contains("B") || selection.contains("b"))
				if (history.size() > 1) {
					history.remove(history.size() - 1);
					history.get(history.size() - 1).printMoves();
				} else
					System.out
							.println("You cannot go back one move because this is the original state of the board.");
			else {
				try {
					int selectedMove = Integer.parseInt(selection);
					Vector<NumberedMove> moves = history.get(history.size() - 1).getLegalMoves();
					boolean moved = false;
					for (NumberedMove x : moves)
						if (selectedMove == x.getIDNumber()) {
							if(x.getMove().getClass()==PawnPromotionMove.class){
								((PawnPromotionMove)(x.getMove())).selectPromotion(newline);
							}
							history.add(new Turn(x.getMove().getBoardAfterMove(),x.getMove().getNewGameState(history.get(history.size()-1).getGameState())));
							history.get(history.size() - 1).printMoves();
							moved = true;
						}
					if (!moved)
						System.out
								.println("It seems that your input did not match an available move.");

				} catch (Exception e) {
					System.out
							.println("Sorry, something was not quite right about that.");
					System.out
							.println("Your input must be an integer with no letters or punctuation.");
				}
			}

		}

	}

}
