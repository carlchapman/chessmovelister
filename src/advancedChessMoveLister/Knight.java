package advancedChessMoveLister;

import java.util.Vector;

public class Knight extends Piece{
	public Knight(boolean isWhite, Square initialPosition) {
		super(isWhite, initialPosition);
	}
	
	public Piece getCopyInNewSquare(Square newSquare){
		return new Knight(this.isWhite(),newSquare);
	}
	
	public String toString(){
		return "knight at "+this.getSquare().toString();
	}
	
	public String getLetter(){
		return (this.isWhite())?"N":"n";
	}
	
	public Vector<Square> getAttackedSquares(ChessBoard board) {
		
		Vector<Square> squares = new Vector<Square>();	
		//check north and south squares
		squares=evaluate(squares, 2,-1,board);
		squares=evaluate(squares, 2, 1,board);
		squares=evaluate(squares, -2,-1,board);
		squares=evaluate(squares, -2, 1,board);
		
		//check east and west squares
		squares=evaluate(squares, -1,2,board);
		squares=evaluate(squares, 1,2,board);
		squares=evaluate(squares, -1,-2,board);
		squares=evaluate(squares, 1,-2,board);
		
		return squares;
	}
	//////////////////////////////////////////////
	private Vector<Square> evaluate(Vector<Square> squares, int n, int l,ChessBoard board){
		try {
			Square s = new Square(this.getSquare().getRow()+n, this.getSquare().getColumn()+l);
			Piece p = board.getPiece(s);
			if (p == null||!(p.isWhite() == this.isWhite()))
				squares.add(s);
		} catch (Exception e) {

		}
		return squares;
	}
}
