package advancedChessMoveLister;

import java.util.Vector;

public class King extends Piece{
	public King(boolean isWhite, Square initialPosition) {
		super(isWhite, initialPosition);
	}
	
	public Piece getCopyInNewSquare(Square newSquare){
		return new King(this.isWhite(),newSquare);
	}
	
	public String toString(){
		return "king at "+this.getSquare().toString();
	}
	
	public String getLetter(){
		return (this.isWhite())?"K":"k";
	}
	public Vector<Move> getPossibleMoves(ChessBoard board, GameState gameState){
		Vector<Move> moves = super.getPossibleMoves(board, gameState);
		
		boolean[] castling = gameState.getCastling();
		if (this.isWhite()) {
			//
			if (board.getPiece(0, 6) == null&& board.getPiece(0, 5) == null && castling[3])
				moves.add(new CastleMove(board, this, new Square(0, 6)));
			//
			if (board.getPiece(0, 2) == null && board.getPiece(0, 3) == null && board.getPiece(0, 1) == null && castling[2])
				moves.add(new CastleMove(board, this, new Square(0, 2)));
		} else {
			if (board.getPiece(7, 6) == null
					&& board.getPiece(7, 5) == null && castling[1])
				moves.add(new CastleMove(board,this, new Square(7, 6)));
			if (board.getPiece(7, 2) == null
					&& board.getPiece(7, 3) == null
					&& board.getPiece(7, 1) == null && castling[0])
				moves.add(new CastleMove(board,this,new Square(7, 2)));
		}
		return moves;
	}
	
	public Vector<Square> getAttackedSquares(ChessBoard board) {

		Vector<Square> squares = new Vector<Square>();
		// check squares starting at North and going clockwise
		squares = evaluate(squares, 1, 0,board);
		squares = evaluate(squares, 1, 1,board);
		squares = evaluate(squares, 0, 1,board);
		squares = evaluate(squares, -1, 1,board);
		squares = evaluate(squares, -1, 0,board);
		squares = evaluate(squares, -1, -1,board);
		squares = evaluate(squares, 0, -1,board);
		squares = evaluate(squares, 1, -1,board);
		
		return squares;
	}
///////////////////////////////////////////////////
	private Vector<Square> evaluate(Vector<Square> squares, int n, int l,ChessBoard board) {
		try {
			Square s = new Square(this.getSquare().getRow() + n, this
					.getSquare().getColumn()
					+ l);
			Piece p = board.getPiece(s);
			if (p == null || !(p.isWhite() == this.isWhite()))
				squares.add(s);
		} catch (Exception e) {

		}
		return squares;
	}
}
