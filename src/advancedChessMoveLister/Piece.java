package advancedChessMoveLister;

import java.util.Vector;

public abstract class Piece {
	private boolean isWhite;
	private Square currentPosition;
	
	public Piece(boolean isWhite, Square initialPosition) {
		this.isWhite = isWhite;
		this.currentPosition = initialPosition;
	}

	public abstract String getLetter();
	
	public abstract Piece getCopyInNewSquare(Square destination);
	
	public abstract Vector<Square> getAttackedSquares(ChessBoard board);

	public Square getSquare() {
		return currentPosition;
	}

	public boolean isWhite() {
		return isWhite;
	}
	
	public Vector<Move> getPossibleMoves(ChessBoard board, GameState gameState){
		Vector<Move> moves = new Vector<Move>();
		Vector<Square> squares = getAttackedSquares(board);
		for(Square x:squares){
			if(board.squareIsOccupied(x))
				moves.add(new CaptureMove(board,this,x));
			else
				moves.add(new Move(board, this,x));
		}
		return moves;
		
	}

}
