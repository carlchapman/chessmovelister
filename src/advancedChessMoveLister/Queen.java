package advancedChessMoveLister;

import java.util.Vector;

public class Queen extends Piece{
	public Queen(boolean isWhite, Square initialPosition) {
		super(isWhite, initialPosition);
	}
	
	public Piece getCopyInNewSquare(Square newSquare){
		return new Queen(this.isWhite(),newSquare);
	}
	
	public String toString(){
		return "queen at "+this.getSquare().toString();
	}
	
	public String getLetter(){
		return (this.isWhite())?"Q":"q";
	}
	
	public Vector<Square> getAttackedSquares(ChessBoard board) {

		Vector<Square> squares = new Vector<Square>();
		int number = this.getSquare().getRow();
		int letter = this.getSquare().getColumn();
		
		//check North
		boolean clear = true;
		int x=1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number + x++,letter);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		//check East
		clear = true;
		x=1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number,letter+x++);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		//check South
		clear = true;
		x=-1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number + x--,letter);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		//check West
		clear = true;
		x=-1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number ,letter+x--);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		//check NorthEast
		clear = true;
		x=1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number + x++,letter+(x-1));
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		//check NorthWest
		clear = true;
		x=1;
		int y=-1;
		while (clear && x < 8) {
			try {
				Square s = new Square(number+x++,letter+y--);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		//check SouthWest
		clear = true;
		x=-1;
		y=1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number + x--,letter+y++);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		//check SouthEast
		clear = true;
		x=-1;
		while (clear && x > -8) {
			try {
				Square s = new Square(number+x-- ,letter+x+1);
				Piece p = board.getPiece(s);
				if (p == null)
					squares.add(s);
				else if (p.isWhite() == this.isWhite())
					clear = false;
				else {
					squares.add(s);
					clear = false;
				}
			} catch (Exception e) {

			}
		}
		
		return squares;
	}
}
