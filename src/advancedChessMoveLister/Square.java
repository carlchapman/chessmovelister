package advancedChessMoveLister;

public class Square {
	private int row;
	private int column;

	public Square(int row, int column) {
		if (row < 0 || row > 7 || column < 0 || column > 7)
			throw new IllegalArgumentException("row:"+row+" column:"+column);

		// using row=0, column =0 as square a1 and row=7, column=7 as square h8,
		// row=0, column=1 as square b1
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
	public boolean equals(Object o){
		if(o==null||o.getClass()!=this.getClass())
			return false;
		Square s = (Square)o;
		if(s.row==row&&s.column==column)
			return true;
		else
			return false;
	}
	public String toString(){
		return ""+(char)(column+97)+(char)(row+49);
	}
}
