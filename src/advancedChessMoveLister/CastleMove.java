package advancedChessMoveLister;

public class CastleMove extends Move {

	// could make some public Static finals here or not...
	private int castlingIndex;

	public CastleMove(ChessBoard boardBeforeMove, Piece actor,
			Square destination) {
		super(boardBeforeMove, actor, destination);

		// move the appropriate rook
		Piece[][] pieces = super.getBoardAfterMove().getPieces();
		// black queen side
		if (!boardBeforeMove.getWhiteIsNext()) {
			if (destination.equals(new Square(7, 2))) {
				castlingIndex = 0;
				pieces[7][3] = pieces[7][0]
						.getCopyInNewSquare(new Square(7, 3));
				pieces[7][0] = null;
			} else if (destination.equals(new Square(7, 6))) {
				castlingIndex = 1;
				pieces[7][5] = pieces[7][7]
						.getCopyInNewSquare(new Square(7, 5));
				pieces[7][7] = null;
			}
		} else {
			if (destination.equals(new Square(0, 2))) {
				castlingIndex = 2;
				pieces[0][3] = pieces[0][0]
						.getCopyInNewSquare(new Square(0, 3));
				pieces[0][0] = null;
			} else if (destination.equals(new Square(0, 6))) {
				castlingIndex = 3;
				pieces[0][5] = pieces[0][7]
						.getCopyInNewSquare(new Square(0, 5));
				pieces[0][7] = null;
			} else
				throw new IllegalArgumentException("incorrect castling setup");
		}
		super.getBoardAfterMove().setPieces(pieces);

	}

	public boolean isLegal() {
		Square kingPassesThrough = null;
		if (castlingIndex == 0)
			kingPassesThrough = new Square(7, 3);
		else if (castlingIndex == 1)
			kingPassesThrough = new Square(7, 5);
		else if (castlingIndex == 2)
			kingPassesThrough = new Square(0, 3);
		else
			kingPassesThrough = new Square(0, 5);
		return !super.getBoardAfterMove().findAttackedSquares().contains(
				kingPassesThrough)
				&& super.isLegal();
	}

}
